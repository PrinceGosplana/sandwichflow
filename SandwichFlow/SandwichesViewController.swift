//
//  SandwichesViewController.swift
//  SandwichFlow
//
//  Created by Colin Eberhardt on 19/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import Foundation
import UIKit

class SandwichesViewController: UITableViewController {
  
  private var sandwiches: [AnyObject] {
    let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
    return appDelegate.sandwiches
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    let headerView = UIView(frame: CGRectMake(0, 0, 320, 250))
    headerView.backgroundColor = UIColor.clearColor()
    
    let header = UIImageView(image: UIImage(named: "Sarnie"))
    header.center = CGPointMake(220, 190)
    headerView.addSubview(header)
    
    self.tableView.tableHeaderView = headerView
    
    self.tableView.backgroundView = UIImageView(image: UIImage(named: "Background-LowerLayer"))
  }
  
  // MARK: Navigation
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    let sandwichVS = segue.destinationViewController as SandwichViewController
    let path = tableView.indexPathForSelectedRow()
    sandwichVS.sandwich = sandwiches[path!.row] as [String:AnyObject]
  }
  
  // MARK: UITableViewDataSource
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return sandwiches.count
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier("Cell")! as UITableViewCell
    cell.backgroundColor = UIColor(white: 1.0, alpha: 0.5)
    
    let sandwich = sandwiches[indexPath.row] as [String:AnyObject]
    cell.textLabel!.text = sandwich["title"] as? String
    
    return cell
  }
}