//
//  CollectionViewLabelCell.swift
//  SandwichFlow
//
//  Created by Colin Eberhardt on 19/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import Foundation
import UIKit

class CollectionViewLabelCell: UICollectionViewCell {
  
  var titleLabel: UILabel!
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    // add a label to the cell
    titleLabel = UILabel(frame: CGRectInset(self.bounds, 3.0, 3.0))
    titleLabel.textAlignment = .Center
    titleLabel.font = UIFont.systemFontOfSize(12.0)
    self.contentView.addSubview(titleLabel)
    
    // make it a rounded rectangle
    self.layer.cornerRadius = 5
    self.layer.masksToBounds = true
    self.layer.backgroundColor = UIColor(white: 0.8, alpha: 1.0).CGColor
  }

  required init(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}