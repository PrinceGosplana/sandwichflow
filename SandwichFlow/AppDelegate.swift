//
//  AppDelegate.swift
//  SandwichFlow
//
//  Created by Colin Eberhardt on 18/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  
  lazy var sandwiches: [AnyObject] = {
    let path = NSBundle.mainBundle().pathForResource("Sandwiches", ofType: "json")
    let data = NSString(contentsOfFile: path!, encoding: NSUTF8StringEncoding, error: nil)
    
    let results = data?.dataUsingEncoding(NSUTF8StringEncoding)
    let json: [AnyObject]! = NSJSONSerialization.JSONObjectWithData(results!, options: NSJSONReadingOptions.MutableContainers, error: nil) as [AnyObject]
    return json
  }()

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    return true
  }
  
}

