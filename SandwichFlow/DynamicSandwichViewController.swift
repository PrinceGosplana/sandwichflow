//
//  DynamicSandwichViewController.swift
//  SandwichFlow
//
//  Created by Александр Исаев on 17.03.15.
//  Copyright (c) 2015 Colin Eberhardt. All rights reserved.
//

import Foundation
import UIKit

class DynamicSandwichViewController: UIViewController, UICollisionBehaviorDelegate {
    
    private var views = [UIView]()
    
    private var sandwiches: [[String: AnyObject]] {
        let appDelegate = UIApplication.sharedApplication().delegate as AppDelegate
        return appDelegate.sandwiches as [[String:AnyObject]]
    }
    private var draggingView = false
    private var previousTouchPoint = CGPoint.zeroPoint
    private let gravity = UIGravityBehavior()
    lazy private var animator: UIDynamicAnimator = {
        return UIDynamicAnimator(referenceView: self.view)
    }()
    private var viewDocked = false
    private var snap: UISnapBehavior!
    
    override func viewDidLoad() {
        
        // 1 add the lower background image
        let backgroundImageView = UIImageView(image: UIImage(named: "Background-LowerLayer.png"))
        backgroundImageView.frame = CGRectInset(self.view.frame, -50, -50)
        view.addSubview(backgroundImageView)
        addMotionEffectToView(backgroundImageView, magnitude: 50)
        
        // 2 add the background mid layer
        let midLayerImageView = UIImageView(image: UIImage(named: "Background-MidLayer.png"))
        view.addSubview(midLayerImageView)
        
        // 3 add foreground image
        let header = UIImageView(image: UIImage(named: "Sarnie.png"))
        header.center = CGPointMake(220, 190)
        view.addSubview(header)
        addMotionEffectToView(header, magnitude: -20)
        
        animator.addBehavior(gravity)
        gravity.magnitude = 4.0
        
        var offset: CGFloat = 250.0
        for sandwich in sandwiches {
            views.append(addRecipeAtOffset(offset, sandwich: sandwich))
            offset -= 50.0
        }
    }
    
    func addMotionEffectToView(view: UIView, magnitude: Double) {
        let xMotion = UIInterpolatingMotionEffect(keyPath: "center.x", type: .TiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = -magnitude
        xMotion.maximumRelativeValue = magnitude
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "center.y", type: .TiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = -magnitude
        yMotion.maximumRelativeValue = magnitude
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [xMotion, yMotion]
        
        view.addMotionEffect(group)
    }
    
    func addRecipeAtOffset(offset: CGFloat, sandwich: [String: AnyObject]) -> UIView {
        let frameForView = CGRectOffset(self.view.bounds, 0.0, self.view.bounds.height - offset)
        
        // create the view controller
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewControllerWithIdentifier("SandwichVC") as SandwichViewController
        
        // set the frame and provide some data
        viewController.sandwich = sandwich
        let childView = viewController.view
        childView.frame = frameForView
        
        // add as a child
        addChildViewController(viewController)
        view.addSubview(viewController.view)
        viewController.didMoveToParentViewController(self)
        
        // add a gesture recognizer
        let pan = UIPanGestureRecognizer(target: self, action: "handlePan:")
        viewController.view.addGestureRecognizer(pan)
        
        // create a collision
        let collision = UICollisionBehavior(items: [childView])
        animator.addBehavior(collision)
        
        let boundaryStart2 = CGPointMake(0, 0)
        let boundaryEnd2 = CGPointMake(self.view.bounds.size.width, 0)
        collision.addBoundaryWithIdentifier("2", fromPoint: boundaryStart2, toPoint: boundaryEnd2)
        collision.collisionDelegate = self
        
        // lower boundary, where the tab rests
        let boundary = childView.frame.origin.y + childView.frame.size.height + 1
        let boundaryStart = CGPointMake(0, boundary)
        let boundaryEnd = CGPointMake(self.view.bounds.size.width, boundary)
        
        collision.addBoundaryWithIdentifier("1", fromPoint: boundaryStart, toPoint: boundaryEnd)
        
        // apply some gravity
        gravity.addItem(childView)
        
        let itemBehavior = UIDynamicItemBehavior(items: [childView])
        animator.addBehavior(itemBehavior)
        
        return childView
    }
    
    private func itemBehaviorForView(view: UIView) -> UIDynamicItemBehavior? {
        for behavior in animator.behaviors {
            if let itemBehavior = behavior as? UIDynamicItemBehavior {
                if itemBehavior.items.first === view {
                    return itemBehavior
                }
            }
        }
        return nil
    }
    
    func handlePan(gesture: UIPanGestureRecognizer) {
        let touchPoint = gesture.locationInView(view)
        let draggedView = gesture.view!
        
        if gesture.state == .Began {
            // was the pan initiated from the top of the recipe?
            let dragStartLocation = gesture.locationInView(draggedView)
            if dragStartLocation.y < 200 {
                draggingView = true
                previousTouchPoint = touchPoint
            }
        } else if gesture.state == .Changed && draggingView {
            // handle dragging
            let yOffset = previousTouchPoint.y - touchPoint.y
            draggedView.center = CGPointMake(draggedView.center.x, draggedView.center.y - yOffset)
            previousTouchPoint = touchPoint
        } else if gesture.state == .Ended && draggingView {
            tryDockView(draggedView)
            addVelocityToView(draggedView, gesture: gesture)
            animator.updateItemUsingCurrentState(draggedView)
            draggingView = false
        }
    }
    
    private func addVelocityToView(view: UIView, gesture: UIPanGestureRecognizer) {
        let vel = CGPointMake(0, gesture.velocityInView(self.view).y)
        let behavior = itemBehaviorForView(view)
        behavior?.addLinearVelocity(vel, forItem: view)
    }
    
    private func tryDockView(view: UIView) {
        let viewHasReachedDockLocation = view.frame.origin.y < 100.0
        if viewHasReachedDockLocation {
            if !viewDocked {
                snap = UISnapBehavior(item: view, snapToPoint: self.view.center)
                animator.addBehavior(snap)
                setAlphaWhenViewDocked(view, alpha: 0.0)
                viewDocked = true
            }
        } else {
            if viewDocked {
                animator.removeBehavior(snap)
                setAlphaWhenViewDocked(view, alpha: 1.0)
                viewDocked = false
            }
        }
    }
    
    func setAlphaWhenViewDocked(view: UIView, alpha: CGFloat) {
        for aView in views.filter({$0 !== view}) {
            aView.alpha = alpha
        }
    }
    
    func collisionBehavior(behavior: UICollisionBehavior, beganContactForItem item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying, atPoint p: CGPoint) {
        let stringIdentifier = identifier as String
        if "2" == stringIdentifier {
            tryDockView(item as UIView)
        }
    }
}