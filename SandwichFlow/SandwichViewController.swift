//
//  ViewController.swift
//  SandwichFlow
//
//  Created by Colin Eberhardt on 18/11/2014.
//  Copyright (c) 2014 Colin Eberhardt. All rights reserved.
//

import UIKit

class SandwichViewController: UIViewController, UICollectionViewDataSource {
  
  var sandwich: [String:AnyObject]!
  
  var keywords: [String] {
    return sandwich["keywords"]! as [String]
  }

  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var keywordCollectionView: UICollectionView!
  @IBOutlet weak var instructionTextView: UITextView!
  @IBOutlet weak var closeButton: UIBarButtonItem!
  @IBOutlet weak var navigationBar: UINavigationBar!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    // add the background
    let background = UIImageView(image: UIImage(named: "Background-LowerLayer"))
    background.alpha = 0.5
    self.view.addSubview(background)
    self.view.sendSubviewToBack(background)
    
    // configure the collection view
    keywordCollectionView.registerClass(CollectionViewLabelCell.self, forCellWithReuseIdentifier: "cell")
    keywordCollectionView.dataSource = self
    keywordCollectionView.backgroundColor = UIColor.clearColor()
    
    let flowLayout = keywordCollectionView.collectionViewLayout as UICollectionViewFlowLayout
    flowLayout.scrollDirection = .Horizontal
    flowLayout.itemSize = CGSizeMake(120, 20)
    
    updateControlsWithYummySandwichData()
    
  }
  
  private func updateControlsWithYummySandwichData() {
    let imageName = sandwich["image"]! as String
    let image = UIImage(named: imageName)
    imageView.image = image
    
    navigationBar.topItem?.title = sandwich["title"]! as? String
    
    let instructions = sandwich["instructions"]! as NSArray
    instructionTextView.text = instructions.componentsJoinedByString("\n\n")
  }

  @IBAction func closeButtonTapper(sender: AnyObject) {
    dismissViewControllerAnimated(true, completion: nil)
  }
  
  // Mark UICollectionViewDataSource methods
  
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return keywords.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as CollectionViewLabelCell
    cell.titleLabel.text = keywords[indexPath.row]
    return cell
  }
  
  
}

